#include<iomanip>
#include<iostream>
#include<string>
using namespace std;
const int primeCount = 1000;
const int borderOfTime = 2400;
void getTasks(int &, string[], int);
void transcriptTasks(string[], int[], int);
bool getTime(int, int[], int);
float getMinTime(int[], int, int&);
float getMaxTime(int[], int, int&);
int main()
{
	setlocale(LC_ALL, "Russian");
	int count,numberOfMax,numberOfMin, Time[primeCount];
	string Tasks[primeCount];
	float Min, Max;

	cout << "Enter count: ";
	cin >> count;

	getTasks(count, Tasks, primeCount);
	transcriptTasks(Tasks, Time, count);
	if (getTime(borderOfTime, Time, count))
	{
		cout << "�������� ������ ��������� ������" << endl;
	}
	else
	{
		cout << "�������� �� ������ ��������� ������" << endl;
	}
	Min = getMinTime(Time, count, numberOfMin);
	Max = getMaxTime(Time, count, numberOfMax);
	cout << "����� ����� �������� ������ = " << numberOfMin << endl << "����� ����� ��������������� ������ = " << numberOfMax << endl;
	cout.setf(ios::fixed);
	cout << "������������ ����� �������� ������ =" <<setprecision(2)<< Min << endl << "������������ ����� ���������� ������ =" << setprecision(2)<< Max << endl;
	system("pause");
	return 0;
}
void getTasks(int & count, string Tasks[], int primeCount)
{
	for (int i = 0; i < count; i++)
	{
		cin >> Tasks[i];
	}
}
void transcriptTasks(string Tasks[], int Time[], int count)
{
	int hours, minutes, hFirstDigit, hSecondDigit, mFirstDigit, mSecondDigit;
	for (int i = 0; i < count; i++)
	{
		hours = int(Tasks[i][0] - '0') * 10 + int(Tasks[i][1] - '0');
		minutes = int(Tasks[i][3] - '0') * 10 + int(Tasks[i][4] - '0');
		Time[i] = hours * 60 + minutes;
	}
}
bool getTime(int borderOfTime, int Time[], int count)
{
	for (int i = 0; i < count; i++)
	{
		borderOfTime -= Time[i];
		if (borderOfTime < 0)
		{
			return 0;
		}
	}
	return 1;
}
float getMinTime(int Time[], int count, int & number)
{
	float Min = 7000;
	for (int i = 0; i < count; i++)
	{
		if (Time[i] < Min)
		{
			Min = Time[i];
			number = i + 1;
		}
	}
	Min = float(Min / 60);
	return Min;
}
float getMaxTime(int Time[], int count, int & number)
{
	float Max = 0;
	for (int i = 0; i < count; i++)
	{
		if (Time[i] > Max)
		{
			Max = Time[i];
			number = i+1;
		}
	}
	Max = float(Max / 60);
	return Max;
}